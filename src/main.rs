use std::io::stdin;
use rand::Rng;

fn random_number() -> i32 {
    let mut rng = rand::thread_rng();
    return rng.gen_range(1..101);
}

fn main() {
    let number = random_number();

    println!("Welcome to the Rust version of guess the number!");
    println!("You'll be given 10 chances to guess the number. The number is between 1 and 100.");

    for i in 0..10 {
        println!("\nEnter the number: ");

        let mut guess = String::new();
        stdin().read_line(&mut guess);
        let actual_guess: i32 = guess.trim().parse().expect("Invalid input!");

        if actual_guess == number {
            println!("You won!");
            break;
        }

        else if actual_guess > number {
            println!("Decrease the number.")
        }

        else if actual_guess < number {
            println!("Increase the number.")
        }

        if i == 9 {
            println!("You lost! The correct answer was {}.", number)
        }

    }

    println!("Thanks for playing this game!");
}
